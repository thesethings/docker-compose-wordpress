# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* docker-compose with MySQL, WordPress, wp-cli and PhpMyAdmin

### How to use ###

* Modify variables with your own given credentials, etc
* Don't miss the domain and admin configuration within wp-cli command
    * Start all container

*       docker-compose up -d

    * Stop all container

*       docker-compose down

    * See container logs 
 
 *      docker logs <containerId or containernname>

      * Connect into Container
      
*       docker exec -it <containerId or containernname> /bin/bash    

#### Links ####
* [docker-compose command-line reference](https://docs.docker.com/compose/reference/)
* [docker command-line reference](https://docs.docker.com/engine/reference/commandline/docker/)
* [wp-cli command-line reference](https://developer.wordpress.org/cli/commands/)
